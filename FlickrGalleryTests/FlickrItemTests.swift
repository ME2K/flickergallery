//
//  FlickrItemTests.swift
//  FlickrGalleryTests
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import XCTest
@testable import FlickrGallery
import SwiftyJSON

class FlickrItemTests: XCTestCase {
    
    var testModel: FlickrItem!
    
    override func setUp() {
        super.setUp()
        let baseLine = FlickrItem()
        baseLine.author = "nobody@flickr.com (\"alamsterdam\")"
        baseLine.dateTaken = Formatter.iso2DateFrom(json: "2017-12-13T13:57:47Z")
        baseLine.directLink = "https://www.flickr.com/photos/alamsterdam/38144854525/"
        baseLine.mediaUrl = "https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg"
        baseLine.title = "Winter in Amsterdam."
        baseLine.itemDescription = "<p><a href=\"https://www.flickr.com/people/alamsterdam/\">alamsterdam</a> posted a photo:</p> <p><a href=\"https://www.flickr.com/photos/alamsterdam/38144854525/\" title=\"Winter in Amsterdam.\"><img src=\"https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg\" width=\"240\" height=\"160\" alt=\"Winter in Amsterdam.\"/></a></p>"
        self.testModel = baseLine
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
    }
    
    func testJsonParse() {
        let jsonString = "{\"title\": \"Winter in Amsterdam.\",\"link\": \"https://www.flickr.com/photos/alamsterdam/38144854525/\",\"media\": {\"m\":\"https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg\"},\"date_taken\": \"2017-12-11T15:47:31-08:00\",\"description\": \" <p><a href=\"https://www.flickr.com/people/alamsterdam/\">alamsterdam</a> posted a photo:</p> <p><a href=\"https://www.flickr.com/photos/alamsterdam/38144854525/\" title=\"Winter in Amsterdam.\"><img src=\"https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg\" width=\"240\" height=\"160\" alt=\"Winter in Amsterdam.\"/></a></p>\",\"published\": \"2017-12-13T13:57:47Z\",\"author\": \"nobody@flickr.com (\"alamsterdam\")\",\"author_id\": \"47086820@N03\",\"tags\": \"amsterdam staatsliedenbuurt cars bikes christmaslights snow winter buildings\"}"
        if let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false) {
            if let json = try? JSON(data: dataFromString), let item = FlickrItem.fromJson(json: json) as? FlickrItem {
                XCTAssertTrue(item.title == testModel.title && item.author == testModel.author && item.itemDescription == testModel.itemDescription && item.dateTaken == testModel.dateTaken && item.directLink == testModel.directLink && item.mediaUrl == testModel.mediaUrl
                )
            }
        }
        
        XCTAssertFalse(false)
    }
}
