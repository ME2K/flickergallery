
//
//  DataSourceTests.swift
//  FlickrGalleryTests
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class DelegateClass: DataSourceDelegate {
    
    var asyncExpectation: XCTestExpectation?
    var wasDelegateCalled: Bool = false
    
    func dataSourceDidUpdate(dataSource: DataSourceProtocol) {
        wasDelegateCalled = true
        asyncExpectation?.fulfill()
    }
    
    func dataSourceDidFailToUpdate(error: Error?, httpCode: Int?, dataSource: DataSourceProtocol) {
        wasDelegateCalled = true
        asyncExpectation?.fulfill()
    }
}

class DataSourceTests: XCTestCase {
    
    var dataSource: GalleryCollectionViewDataSource!
    var delegateClass: DelegateClass!
    override func setUp() {
        super.setUp()
        dataSource = GalleryCollectionViewDataSource()
        delegateClass = DelegateClass()
        dataSource.delegate = delegateClass
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func search(withSearchTerm searchTerm: String?) {
        let expectation = self.expectation(description: "testing Search")
        delegateClass.asyncExpectation = expectation
        dataSource.performSearch(withSearchString: searchTerm)
        waitForExpectations(timeout: 10.0) { (error) in
            if let error = error {
                XCTFail("Error testing search with no term: \(error)")
            }
            guard self.delegateClass.wasDelegateCalled else {
                XCTFail("delegate method not called")
                return
            }
            
            XCTAssertTrue(self.dataSource.items.count > 0)
        }
    }
    
    func testSearchWithNoTerm() {
        self.search(withSearchTerm: nil)
    }
    
    func testSearchWithSearchTermTag() {
        self.search(withSearchTerm: "House")
    }
    
    func testSearchWithSearchTermMultipleTags() {
        self.search(withSearchTerm: "House, Car, Green")
    }
    
    func testSearchWithSearchTermUID() {
        self.search(withSearchTerm: "35328455@N08")
    }
    
    func testSearchWithSearchTermMultipleUID() {
        self.search(withSearchTerm: "35328455@N08, 77109890@N00")
    }
    
    func testSearchWithSearchTermUIDTag() {
        self.search(withSearchTerm: "tram, 35328455@N08")
    }
}
