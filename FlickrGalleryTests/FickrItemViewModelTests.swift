//
//  FickrItemViewModelTests.swift
//  FlickrGalleryTests
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import XCTest
@testable import FlickrGallery
import SwiftyJSON

class FickrItemViewModelTests: XCTestCase {
    
    var viewModel: FlickrItemViewModel!
    
    override func setUp() {
        super.setUp()
        let baseLine = FlickrItem()
        baseLine.author = "nobody@flickr.com (\"alamsterdam\")"
        baseLine.dateTaken = Formatter.iso2DateFrom(json: "2017-12-13T13:57:47Z")
        baseLine.directLink = "https://www.flickr.com/photos/alamsterdam/38144854525/"
        baseLine.mediaUrl = "https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg"
        baseLine.title = "Winter in Amsterdam."
        baseLine.itemDescription = "<p><a href=\"https://www.flickr.com/people/alamsterdam/\">alamsterdam</a> posted a photo:</p> <p><a href=\"https://www.flickr.com/photos/alamsterdam/38144854525/\" title=\"Winter in Amsterdam.\"><img src=\"https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg\" width=\"240\" height=\"160\" alt=\"Winter in Amsterdam.\"/></a></p>"
        viewModel = FlickrItemViewModel(withFlickrItem: baseLine)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAuthorString() {
        XCTAssertTrue(self.viewModel.authorTitle == "Winter in Amsterdam., alamsterdam")
    }
    
    func testAuthor() {
        XCTAssertTrue(self.viewModel.author == "alamsterdam")
    }
    
    func testTitle() {
        XCTAssertTrue(self.viewModel.title == "Winter in Amsterdam.")
    }
    
    func testDescription() {
        XCTAssertTrue(self.viewModel.itemDescription == "<p><a href=\"https://www.flickr.com/people/alamsterdam/\">alamsterdam</a> posted a photo:</p> <p><a href=\"https://www.flickr.com/photos/alamsterdam/38144854525/\" title=\"Winter in Amsterdam.\"><img src=\"https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg\" width=\"240\" height=\"160\" alt=\"Winter in Amsterdam.\"/></a></p>")
    }
    
    func testDirectLink() {
        let url = URL(string: "https://www.flickr.com/photos/alamsterdam/38144854525/")
        XCTAssertTrue(self.viewModel.directLink == url)
    }
    
    func testMediaUrl() {
        let url = URL(string: "https://farm5.staticflickr.com/4689/38144854525_1a62ce4838_m.jpg")
        XCTAssertTrue(self.viewModel.mediaUrl == url)
    }
    
    func testDateFormat() {
        XCTAssertTrue(self.viewModel.dateCreated == "Wednesday, 13 December 2017")
    }
}
