//
//  APITests.swift
//  FlickrGalleryTests
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import XCTest
@testable import FlickrGallery
import PromiseKit

class APITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRetrieveImages() {
        let expectation = XCTestExpectation(description: "Download images")
        firstly {
            return API.instance.retrieveImages(withTags: ["house"], userIds: [])
        }.then { images -> Void in
            XCTAssertTrue(images.count > 0) //we got houses
            expectation.fulfill()
        }.catch { _ in
            XCTFail()
            expectation.fulfill()
        }
        
        //wait 10 seconds for the API call to fulfill
        wait(for: [expectation], timeout: 10.0)
    }
}
