//
//  RegexTests.swift
//  FlickrGalleryTests
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class RegexTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFlickrRegEx() {
        let input = "35328455@N08"
        XCTAssertTrue(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx() {
        let input = "35328455!N08"
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx2() {
        let input = "35328455"
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx3() {
        let input = "353284551234"
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx4() {
        let input = "00000000@a00"
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx5() {
        let input = ""
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx6() {
        let input = "35328455@N081"
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
    
    func testInvalidFlickrRegEx7() {
        let input = "35328455@N0"
        XCTAssertFalse(Regex.isValidFlickrUID(input: input))
    }
}
