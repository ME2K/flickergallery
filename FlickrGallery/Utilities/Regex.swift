//
//  Regex.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import Foundation

class Regex {
    /** Tests if string is a flickr user id e.g. 35328455@N08 */
    static func isValidFlickrUID(input: String) -> Bool {
        let regEx = "^([0-9]{8})@([A-Z]{1})([0-9]{2})$"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regEx)
        return predicate.evaluate(with: input)
    }
}
