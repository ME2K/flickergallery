//
//  Formatter.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit

public class Formatter {
    
    
    private static let prettyDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEE, dd MMM yyyy"
        return formatter
    }()
    
    private static let fullDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEEE, dd MMMM yyyy"
        return formatter
    }()
    
    //2017-03-17T08:00:00Z
    private static let isoJsonDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"// "yyyy-MM-dd-HH-mm-ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    
    private static let isoJsonDateFormatter2: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    //ISO 8601
    private static let iso8601DateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"// "yyyy-MM-dd-HH-mm-ss"
        dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
        return dateFormatter
    }()
    
    private static let jsonDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    
    /** Gets a nicely formatted time as Mon, 01 Jan 2017 */
    public class func prettyDate(date: Date) -> String {
        return prettyDateFormatter.string(from: date)
    }
    
    /** Formats given date as 'MONDAY, 23rd MARCH 2017'  */
    public class func fullDate(date: Date) -> String {
        //"23/10/2015"
        return fullDateFormatter.string(from: date)
    }
    
    /** Converts a json date into a date */
    public class func dateFrom(json: String) -> Date? {
        return jsonDateFormatter.date(from: json)
    }
    
    /** Converts a json date into a date */
    public class func isoDateFrom(json: String) -> Date? {
        return isoJsonDateFormatter.date(from: json)
    }
    
    /** Converts a json date into a date */
    public class func iso2DateFrom(json: String) -> Date? {
        return isoJsonDateFormatter2.date(from: json)
    }
    
    /** Converts a json date into a date */
    public class func iso8601DateFrom(json: String) -> Date? {
        return iso8601DateFormatter.date(from: json)
    }
    
    /** Converts a json date into a string */
    public class func iso8601DateFrom(date: Date) -> String {
        return iso8601DateFormatter.string(from: date)
    }

    
    /** Gets a json string version of the given date */
    public class func jsonDate(fromDate: Date?) -> String {
        guard let date = fromDate else { return String() }
        return jsonDateFormatter.string(from: date)
    }
}



