//
//  FlickrItemViewModel.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit

class FlickrItemViewModel: NSObject {
    
    // MARK: Properties
    
    private var model: FlickrItem!
    
    init(withFlickrItem flickrItem: FlickrItem) {
        self.model = flickrItem
    }
    
    var title: String? {
        return self.model.title
    }
    
    var authorTitle: String? {
        if let imageTitle = self.model.title {
            return "\(imageTitle), \(self.author ?? "")"
        }
        return self.author

    }
    
    var itemDescription: String? {
        return self.model.itemDescription
    }
    
    var author: String? {
        return self.model.author?.replacingOccurrences(of: "nobody@flickr.com (\"", with: "").replacingOccurrences(of: "\")", with: "")
    }
    
    var mediaUrl: URL? {
        guard let mediaString = self.model.mediaUrl else {
            return nil
        }
        return URL(string: mediaString)
    }
    
    var directLink: URL? {
        guard let directLink = self.model.directLink else {
            return nil
        }
        return URL(string: directLink)
    }
    
    var dateCreated: String? {
        if let date = self.model.dateTaken {
            return Formatter.fullDate(date: date)
        }
        return nil
    }
    
}
