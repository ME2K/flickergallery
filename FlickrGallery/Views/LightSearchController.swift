//
//  LightSearchController.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit


/** Search cvontroller with a light status bar style */
class LightSearchController: UISearchController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
