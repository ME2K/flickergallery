//
//  NoSearchResultsCollectionViewCell.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit

protocol NoSearchResultsCollectionViewCellDelegate {
    func noSearchResultsCellRetryPressed(_ cell: NoSearchResultsCollectionViewCell)
}

class NoSearchResultsCollectionViewCell: UICollectionViewCell, RegisterableCollectionViewCell {

    // MARK: Constants
    
    class var identifier: String {
        return String(describing: NoSearchResultsCollectionViewCell.self)
    }
    
    // MARK: Properties
    
    var delegate: NoSearchResultsCollectionViewCellDelegate?
    
    // MARK: Outlets
    
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var retryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        setUp()
    }
    
    func setUp() {
        retryLabel.text = NSLocalizedString("no_search_results_cell_title", value: "No Images For Search Criteria", comment: "No search results title")
        retryButton.setTitle(NSLocalizedString("no_search_results_cell_retry_button_title", value: "Search Again", comment: "Search Again button title"), for: .normal)
    }
    
    @IBAction func retryPressed(withSender sender: UIButton) {
        self.delegate?.noSearchResultsCellRetryPressed(self)
    }
}
