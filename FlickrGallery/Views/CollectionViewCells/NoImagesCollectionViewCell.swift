//
//  NoImagesCollectionViewCell.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit

class NoImagesCollectionViewCell: NoSearchResultsCollectionViewCell {

    // MARK: Constants
    
    override class var identifier: String {
        return String(describing: NoImagesCollectionViewCell.self)
    }
    
    // MARK: Methods
    
    override func setUp() {
        retryLabel.text = NSLocalizedString("no_images_cell_title", value: "Unable To Get Images", comment: "Unable to get images title")
        retryButton.setTitle(NSLocalizedString("no_images_cell_retry_button_title", value: "Retry", comment: "Retry button title"), for: .normal)
    }

}
