//
//  ImageCollectionViewCell.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit
import AlamofireImage

class ImageCollectionViewCell: UICollectionViewCell, RegisterableCollectionViewCell {
    
    //MARK: Constants
    
    static var identifier: String {
        return String(describing: ImageCollectionViewCell.self)
    }
    
    static var height: CGFloat {
        return 200.0
    }
    
    //MARK: Outlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    //MARK: Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        reset()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        reset()
    }
    
    func reset() {
        self.imageView.image = nil
        self.titleLabel.text = nil
        self.dateLabel.text = nil
    }
    
    func update(withViewModel viewModel: FlickrItemViewModel) {
        if let mediaUrl = viewModel.mediaUrl {
            self.imageView.af_setImage(withURL: mediaUrl, placeholderImage: #imageLiteral(resourceName: "placeholder_200x100"))
        }
        self.titleLabel.text = viewModel.authorTitle
        self.dateLabel.text = viewModel.dateCreated
    }
}

