//
//  SegueIdentifiers.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import Foundation

enum SegueIdentifiers: String, SegueIdentityProvider {
    case showWebViewController = "SegueShowWebViewController"
}
