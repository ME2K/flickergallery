//
//  Constants.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import Foundation

struct Constants {
    static var baseAPIUrl = "https://api.flickr.com"
}
