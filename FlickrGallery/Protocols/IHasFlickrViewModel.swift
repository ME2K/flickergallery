//
//  IHasFlickrViewModel.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import Foundation

/** Protocol for passing FlickrItem model around between views */
protocol IHasFlickrViewModel {
    var viewModel: FlickrItemViewModel? { get set }
}
