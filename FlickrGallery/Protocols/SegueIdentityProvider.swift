//
//  SegueIdentityProvider.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import Foundation

/** Protocol for organising segues */
protocol SegueIdentityProvider {
    var rawValue: String { get }
}
