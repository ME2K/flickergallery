//
//  CollectionViewDataSource.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit

/** Protocol for adding collection view methods to data sources */
protocol CollectionViewDataSource {
    func numberOfSections() -> Int
    func numberOfRows(inSection section: Int) -> Int
    func registerCells(withCollectionView collectionView: UICollectionView)
    func cellForRow(atIndexPath indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell
    func sizeForCell(atIndexPath indexPath: IndexPath, collectionView: UICollectionView) -> CGSize
    func willDisplayCell(atIndexPath indexPath: IndexPath, cell: UICollectionViewCell, collectionView: UICollectionView)
}
