//
//  GalleryCollectionViewController.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit

class GalleryCollectionViewController: UICollectionViewController {
    
    // MARK: Properties
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    fileprivate var dataSource = GalleryCollectionViewDataSource()
    fileprivate var searchController: LightSearchController!
    
    // MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .black
        self.clearsSelectionOnViewWillAppear = false

        searchController = LightSearchController(searchResultsController: nil)
        searchController.searchBar.tintColor = .white
        searchController.dimsBackgroundDuringPresentation = false
        //as we are using iOS 11 let's use the new navigationItem searchController property
        self.navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        self.definesPresentationContext = true
        
        dataSource.registerCells(withCollectionView: self.collectionView!)
        dataSource.delegate = self
        self.performSearch(withSearchString: nil)
    }
    
    private func performSearch(withSearchString searchString: String?) {
        collectionView?.reloadData()
        collectionView?.collectionViewLayout.invalidateLayout()
        dataSource.performSearch(withSearchString: searchString)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if var destinationVC = segue.destination as? IHasFlickrViewModel, let viewModel = sender as? FlickrItemViewModel {
            destinationVC.viewModel = viewModel
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSource.numberOfSections()
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.numberOfRows(inSection: section)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dataSource.cellForRow(atIndexPath: indexPath, collectionView: collectionView)
        if let cell = cell as? NoSearchResultsCollectionViewCell {
            cell.delegate = self
        }
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(.showWebViewController, sender: dataSource.viewModels[indexPath.row])
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        dataSource.willDisplayCell(atIndexPath: indexPath, cell: cell, collectionView: collectionView)
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension GalleryCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return dataSource.sizeForCell(atIndexPath: indexPath, collectionView: collectionView)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

// MARK: DataSourceDelegate

extension GalleryCollectionViewController: DataSourceDelegate {
    
    func dataSourceDidUpdate(dataSource: DataSourceProtocol) {
        self.collectionView?.reloadData()
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func dataSourceDidFailToUpdate(error: Error?, httpCode: Int?, dataSource: DataSourceProtocol) {
        let errorTitle = NSLocalizedString("error_alert_title", value: "Error", comment: "Error Alert Title")
        self.showAlert(title: errorTitle, message: error?.localizedDescription)
        self.collectionView?.reloadData()
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
}

// MARK: UISearchControllerDelegate

extension GalleryCollectionViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.performSearch(withSearchString: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.performSearch(withSearchString: searchBar.text)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.performSearch(withSearchString: searchBar.text)
    }
}

// MARK: NoSearchResultsCollectionViewCellDelegate

extension GalleryCollectionViewController: NoSearchResultsCollectionViewCellDelegate {
    
    func noSearchResultsCellRetryPressed(_ cell: NoSearchResultsCollectionViewCell) {
        if cell is NoImagesCollectionViewCell {
            self.performSearch(withSearchString: dataSource.searchString)
        } else {
            searchController.searchBar.becomeFirstResponder()
        }
    }
}
