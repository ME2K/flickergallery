//
//  WebViewController.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 13/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, IHasFlickrViewModel {
    
    // MARK: Properties
    
    fileprivate var webView: WKWebView!
    fileprivate var initialUrl: URL?
    var viewModel: FlickrItemViewModel?

    // MARK: Outlets
    
    @IBOutlet weak var refreshButton: UIBarButtonItem?
    @IBOutlet weak var forwardButton: UIBarButtonItem?
    @IBOutlet weak var backwardButton: UIBarButtonItem?
    
    // MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the webview
        self.webView = WKWebView(frame: CGRect.zero)
        self.webView.allowsBackForwardNavigationGestures = true
        self.webView.backgroundColor = .black
        self.view.addSubview(self.webView)
        self.webView.translatesAutoresizingMaskIntoConstraints = false
        self.webView.layoutAttachAll(to: self.view)
        self.webView.navigationDelegate = self
        
        self.forwardButton?.isEnabled = false
        self.backwardButton?.isEnabled = false
        self.refreshButton?.isEnabled = false
        
        self.navigationController?.toolbar.tintColor = .white
        self.navigationController?.toolbar.barStyle = .blackTranslucent
        
        self.initialUrl = self.viewModel?.directLink
        self.loadInitialRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setToolbarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setToolbarHidden(true, animated: animated)
    }
    
    private func loadInitialRequest() {
        guard let url = initialUrl else { return }
        let request = URLRequest(url: url)
        webView.load(request)
    }
    
    // MARK: Actions
    
    @IBAction func refreshPressed(withSender sender: UIBarButtonItem) {
        self.webView.reload()
    }
    
    @IBAction func backWebPagePressed(withSender sender: UIBarButtonItem) {
        self.webView.goBack()
    }
    
    @IBAction func forwardWebPagePressed(withSender sender: UIBarButtonItem) {
        self.webView.goForward()
    }
}

// MARK: WKNavigationDelegate

extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.backwardButton?.isEnabled = webView.canGoBack
        self.forwardButton?.isEnabled = webView.canGoForward
        self.refreshButton?.isEnabled = true
    }
}
