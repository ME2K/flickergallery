//
//  FlickrItem.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import SwiftyJSON

class FlickrItem: NSObject, JSONResponseProtocol {
    
    // MARK: Properties
    
    var title: String?
    var directLink: String?
    var mediaUrl: String?
    var dateTaken: Date?
    var itemDescription: String?
    var author: String?
    
    // MARK: Methods
    
    static func fromJson(json: JSON) -> JSONResponseProtocol? {
        let item = FlickrItem()
        item.title = json ~~> "title"
        item.directLink = json ~~> "link"
        if json["media"] != JSON.null {
            item.mediaUrl = json["media"] ~~> "m"
        }
        item.dateTaken = json ~~> "date_taken"
        item.itemDescription = json ~~> "description"
        item.author = json ~~> "author"
        return item
    }
}
