//
//  GalleryCollectionViewDataSource.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import UIKit


// this is the Collection view data source view model, I have decided to inherit it instead of treating it as a stand alone class inintialised with a data source due to the view
class GalleryCollectionViewDataSource: GalleryDataSource, CollectionViewDataSource {
    
    // MARK: Constants
    
    var imageSection: Int = 0
    var noResultsSection: Int = 1
    var loadingSection: Int = 2
    
    // MARK: Methods
    
    func registerCells(withCollectionView collectionView: UICollectionView) {
        ImageCollectionViewCell.register(collectionView)
        NoSearchResultsCollectionViewCell.register(collectionView)
        NoImagesCollectionViewCell.register(collectionView)
        LoadingCollectionViewCell.register(collectionView)
    }
    
    func numberOfSections() -> Int {
        return 3
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        switch section {
        case loadingSection:
            return self.isLoading ? 1 : 0
        case imageSection:
            return self.viewModels.count
        case noResultsSection:
            return (self.viewModels.isEmpty && !self.isLoading) ? 1 : 0
        default:
            return 0
        }
    }
    
    func cellForRow(atIndexPath indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell {
        
        switch indexPath.section {
        case imageSection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.identifier, for: indexPath) as! ImageCollectionViewCell
            cell.update(withViewModel: self.viewModels[indexPath.row])
            return cell
        case noResultsSection:
            if self.searchString != nil {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NoSearchResultsCollectionViewCell.identifier, for: indexPath) as! NoSearchResultsCollectionViewCell
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NoImagesCollectionViewCell.identifier, for: indexPath) as! NoImagesCollectionViewCell
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LoadingCollectionViewCell.identifier, for: indexPath) as! LoadingCollectionViewCell
            return cell
        }
    }
    
    func sizeForCell(atIndexPath indexPath: IndexPath, collectionView: UICollectionView) -> CGSize {
        switch indexPath.section {
        case imageSection:
            return CGSize(width: collectionView.bounds.width, height: ImageCollectionViewCell.height)
        default:
            return collectionView.frame.size
        }
    }
    
    func willDisplayCell(atIndexPath indexPath: IndexPath, cell: UICollectionViewCell, collectionView: UICollectionView) {
        if indexPath.section == loadingSection, let cell = cell as? LoadingCollectionViewCell {
            cell.spinner.startAnimating()
        }
    }
}
