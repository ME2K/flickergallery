//
//  GalleryDataSource.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import PromiseKit

class GalleryDataSource: DataSource<FlickrItem, FlickrItemViewModel> {
    
    // MARK: Properties
    
    var searchString: String?
    
    //MARK: Methods
    
    /** Perform a search agains Flickr's API expected input: cars, bikes, aeroplanes, uid123456, uid492222 */
    func performSearch(withSearchString searchString: String?) {
        self.searchString = searchString
        self.cancelSearch()
        self.isLoading = true
        
        // split the search string into tags and userids
        
        let searchItems = searchString?.replacingOccurrences(of: " ", with: "") .split(separator: ",")
        var tags = [String]()
        var userIds = [String]()
        
        if let searchItems = searchItems {
            for searchItem in searchItems {
                let searchItemString = String(searchItem)
                if Regex.isValidFlickrUID(input: searchItemString) {
                    userIds.append(searchItemString)
                } else {
                    tags.append(searchItemString)
                }
            }
        }
        
        firstly {
            API.instance.retrieveImages(withTags: tags, userIds: userIds)
        }.then { results -> Void in
            self.items = results
            self.populateViewModels()
            self.isLoading = false
            self.delegate?.dataSourceDidUpdate(dataSource: self)
        }.catch { error in
            self.clear()
            self.isLoading = false
            self.delegate?.dataSourceDidFailToUpdate(error: error, httpCode: nil, dataSource: self)
        }
    }
    
    /** Empty the datasource */
    override func clear() {
        self.items.removeAll()
        self.viewModels.removeAll()
    }
    
    /** Build view models with datasource Items */
    private func populateViewModels() {
        self.viewModels.removeAll()
        for item in self.items {
            let viewModel = FlickrItemViewModel(withFlickrItem: item)
            self.viewModels.append(viewModel)
        }
    }
    
    func cancelSearch() {
        if self.isLoading {
            // as we only have one API call, canceling all open requests on our API session manager is fine here.
            API.instance.cancel()
            self.isLoading = false
        }
    }
}
