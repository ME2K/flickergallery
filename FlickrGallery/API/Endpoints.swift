//
//  Endpoints.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import Alamofire


enum Endpoints: URLRequestConvertible {
    
    /** Get images with tags and userIds */
    case publicFeed([String], [String])
    
    // Path for the request
    private var path: String {
        switch self {
        case .publicFeed:
            return "/services/feeds/photos_public.gne"
        }
    }
    
    // Request type, GET by default, if there were other APIs that required PUT ot POST, that would be specified here
    private var methodType: HTTPMethod {
        return .get
    }
    
    // The optional request parameters if required.
    private var parameters: Parameters? {
        switch self {
            
        case .publicFeed(let tags, let userIds):
            return [ "tags" : tags.joined(separator: ","),
                     "ids" : userIds.joined(separator: ","),
                     "format" : "json",
                     "nojsoncallback" : 1
                   ]
        }
    }
    
    // Encoding type for parameters
    private var encodingType: ParameterEncoding {
        switch self {
        case .publicFeed:
            return URLEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try Constants.baseAPIUrl.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        urlRequest.httpMethod = methodType.rawValue
        
        if let parameters = parameters {
            urlRequest = try encodingType.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
    
}


