//
//  API.swift
//  FlickrGallery
//
//  Created by Myles Eynon on 12/12/2017.
//  Copyright © 2017 MylesEynon. All rights reserved.
//

import PromiseKit
import SwiftyJSON
import Alamofire

class API {
    
    static let instance = API() //we use a static class to keep the sessionManager alive for the duration of the app.
    
    private var sessionManager: SessionManager = {
        return API.createSession()
    }()
    
    
    static private func createSession() -> SessionManager {
        debugPrint("sessionManager - createSession")
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForResource = 60.0
        configuration.timeoutIntervalForRequest = 60.0
        
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        return sessionManager
    }
    
    /** Cancels all API requests on the session manager, and recreates a new manager */
    func cancel() {
        debugPrint("API - cancel")
        sessionManager.session.invalidateAndCancel() //this action requires the session to be recreated
        sessionManager = API.createSession()
    }
    
    /** Perorm an API request */
    internal static func request(_ endpoint: Endpoints) -> Promise<JSON> {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        return Promise<JSON> { fulfil, reject in
            
            let responseBlock = { (response: DataResponse<Data>) in
                
                switch response.result {
                case .success:
                    
                    if let result = response.result.value, let json = try? JSON(data: result) {
                        fulfil(json)
                    } else {
                        reject(APIErrors.noData)
                    }
                    
                case .failure(let error) :
                    debugPrint("call failed: \(error)")
                    reject(error)
                }
                
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
            
            instance.sessionManager.request(endpoint).validate().responseData(completionHandler: responseBlock)
        }
    }
}

extension API {
    
    /** Perform a search agains Flickr's API. expected input: cars, bikes, aeroplanes, uid123456, uid492222 */
    func retrieveImages(withTags tags: [String], userIds: [String]) -> Promise<[FlickrItem]> {
        return Promise { fulfil, reject in

            debugPrint("retrieving Flickr Images...")
            
            API.request(.publicFeed(tags, userIds)).then { json -> Void in
                
                debugPrint("...Flickr Images Downloaded")
                let itemsJson = json["items"]
                if itemsJson != JSON.null, let flickrItemsJson = itemsJson.array {
                    var items = [FlickrItem]()
                    for flickrItemJson in flickrItemsJson {
                        if let flickrItem = FlickrItem.fromJson(json: flickrItemJson) as? FlickrItem {
                            items.append(flickrItem)
                        }
                    }
                    fulfil(items)
                } else {
                    reject(APIErrors.noData)
                }
            }.catch { error in
                reject(error)
            }
        }
    }
    
    /** Convert errors into a user friendly display */
    static func showErrorAlertView(withViewController viewController: UIViewController?, error: Error?, serverMessage: String? = nil, action: ((_ action: UIAlertAction) -> Void)? = nil) {
        var title = NSLocalizedString("error_title", value: "Error", comment: "Error")
        var body = NSLocalizedString("error_server_body", value: "Flickr is currently experiencing some server issues, please try again later.", comment: "Server Issues alert")
        
        let networkErrorTitle = NSLocalizedString("error_network_title", value: "Network Error", comment: "Network Error")
        let serverErrorTitle = NSLocalizedString("error_server_title", value: "Server Error", comment: "Server Error")

        if ReachabilityManager.instance.status == .notReachable || ReachabilityManager.instance.status == .unknown {
            title = networkErrorTitle
            body = NSLocalizedString("error_network_no_connection_body", value: "Unable to connect to the internet, please check and try again later.", comment: "Unable to connect to the internet alert")
        } else {
            if let error = error {
                switch (error as NSError).code {
                case NSURLErrorTimedOut:
                    title = networkErrorTitle
                    body = NSLocalizedString("error_timeout_body", value: "Request timed out, please try again.", comment: "Request timed out, please try again.")
                default:
                    title = serverErrorTitle
                }
            } else {
                title = serverErrorTitle
            }
        }
        
        if let serverMessage = serverMessage {
            title = serverMessage
        }
        
        if let viewController = viewController {
            viewController.showAlert(title: title, message: body, okHandler: action)
        }
    }
}


